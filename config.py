# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     config
   Description :
   Author :       潘晓华
   date：          2018/7/23
-------------------------------------------------
"""

import os
class Config(object):
    OPENSHIFT_URL = os.getenv('OPENSHIFT_URL', 'https://demo.master.openshift.com:8443')
    OPENSHIFT_USER = os.getenv('OPENSHIFT_USER', 'admin')
    OPENSHIFT_PASSWD = os.getenv('OPENSHIFT_PASSWD', 'password')

